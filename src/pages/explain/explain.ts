import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PlaceInterface} from "../../library/storage/place.interface";
import {PlaceFactory} from "../../library/storage/place-factory";
import {BasePage} from "../page";
import {AuthServiceProvider} from "../../providers/service/auth.service";


@IonicPage({name:'tutorial'})
@Component({
  selector: 'page-explain',
  templateUrl: 'explain.html',
})
export class ExplainPage extends BasePage{

   private place:PlaceInterface = null;

   public slides = [
        {
            title: "진짜 스터디 RealTudy!",
            description: " <b>RealTudy</b>란 진정한 모임의 의미를 말합니다. 무한정 공부가 아닌 특정한 목표를 정해 모임의 의미를 찾아 드립니다.",
            image: "assets/images/explain/1.jpg",
        }
    ];


  constructor(public navCtrl: NavController,protected authService:AuthServiceProvider, public navParams: NavParams) {
      super(authService);

      this.place = PlaceFactory.instance().factory('local');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExplainPage');
  }


  public move(){
      this.place.set('explain', true);
      this.navCtrl.setRoot(this.$PAGE.TABS);
  }

}
