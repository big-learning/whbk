import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExplainPage } from './explain';
import {AuthServiceProvider} from "../../providers/service/auth.service";

@NgModule({
  declarations: [
    ExplainPage,
  ],
  imports: [
    IonicPageModule.forChild(ExplainPage),
  ],
  providers: [
      AuthServiceProvider
  ]
})
export class ExplainPageModule {}
