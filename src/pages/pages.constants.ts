import {PageInterface} from "../models/page/page";

export const PAGE  = {

    TABS : 'tabs',  //전체 탭

    TUTORIAL : 'tutorial', //시작 튜토리얼

    HOME : 'home', //메인화면

    LOGIN : 'login', //로그인
    REGISTER : 'register', //회원가입

    MYPAGE : 'mypage', //마이 페이지
    USER_SEL_CATEGORY : 'user-sel-category', //유저 카테고리 선택
    USER_CATEGORY : 'user-category', //유저 카테고리 리스트 

    STUDY_LIST :'study-list', //그룹 리스트
    STUDY_VIEW :'study-view', //그룹 보기
    STUDY_CATEGORY: 'study-category', //그룹 카테고리,

    CREATE_STUDY :'create-study', //그룹 생성페이지
    CREATE_NEXT_STUDY :'create-next-study', //그룹 생성페이지2

};



export const PAGE_LIST: PageInterface[] = [
    { name: '메인', page: PAGE.TABS, tab: PAGE.HOME, index: 0, icon: 'calendar' },
    { name: '스터디 리스트', page: PAGE.TABS, tab: PAGE.STUDY_LIST, index: 1, icon: 'calendar' },
    { name: '마이페이지', page: PAGE.TABS, tab: PAGE.MYPAGE, index: 2, icon: 'calendar' },
    { name: '스터디 생성', page: PAGE.CREATE_STUDY, icon: 'calendar' },
];