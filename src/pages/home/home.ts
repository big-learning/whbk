import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {BasePage} from "../page";
import {RootNavProvider} from "../../providers/helper/root-nav";
import {GroupUserServiceProvider} from "../../providers/service/group/group-user.service";
import {Group} from "../../models/group/group";
import {AuthServiceProvider} from "../../providers/service/auth.service";

@IonicPage({name:'home'})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage extends BasePage{

    private groupList:Array<Group> = [];

    private loading = true;

    constructor(
        protected authService:AuthServiceProvider,
        public navCtrl: NavController,
        private rootNav:RootNavProvider,
        private groupUserServiceProvider:GroupUserServiceProvider
    ) {
        super(authService);

        if(this.isAuthenticated()){
            this.list();
        }
    }

    public list(){
        this.groupUserServiceProvider.list((datas)=>{

            for(let i in datas['list']){
                let tempDateTime = datas['list'][i]['GroupStartDatetime'];
                datas['list'][i]['GroupStartDatetime'] = tempDateTime.substring(0,4)+"년 "+tempDateTime.substring(5,7)+"월 "+tempDateTime.substring(6,8)+"일 ";
            }
            this.groupList = datas['list'];

            this.loading = false;
        })
    }

    public studyLink(group){
        this.rootNav.rootPush(this.$PAGE.STUDY_VIEW,{group});
    }
}
