import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from "ionic-angular";
import {HomePage} from "./home";
import {ComponentsModule} from "../../components/components.module";
import {GroupUserServiceProvider} from "../../providers/service/group/group-user.service";
import {RootNavProvider} from "../../providers/helper/root-nav";

@NgModule({
  imports: [
      CommonModule,
      ComponentsModule,
      IonicPageModule.forChild(HomePage)
  ],
  declarations: [
      HomePage,
  ],
  exports:[
      HomePage,
  ],
  providers:[
      RootNavProvider,
      GroupUserServiceProvider
  ]
})
export class HomeModule {}
