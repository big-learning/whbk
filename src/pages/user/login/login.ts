import {Component, OnInit} from "@angular/core";
import {AlertController, IonicPage, NavController, NavParams} from "ionic-angular";
import {AuthPage} from "../../page";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthServiceProvider} from "../../../providers/service/auth.service";
import {HelperProvider} from "../../../providers/helper/helper";
import {User} from "../../../models/user";
import {PlaceInterface} from "../../../library/storage/place.interface";
import {PlaceFactory} from "../../../library/storage/place-factory";

@IonicPage({name: 'login', defaultHistory:['home']})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage extends AuthPage implements OnInit{

    private loginForm:FormGroup = null;
    private place:PlaceInterface = null;

    private cid = '';

    constructor(
        protected authService:AuthServiceProvider,
        private formBuilder: FormBuilder,
        private navCtrl:NavController,
        private navParams: NavParams,
        private helper:HelperProvider,
    ){
        super(authService);

        this.loginForm = this.formBuilder.group({
            Email:['', Validators.required],
            Password:['',Validators.required]
        });

        this.place = PlaceFactory.instance().factory('cookie');
    }

    public ngOnInit(){
        this.cid  = this.place.get('cid');
    }


    public onLogin(){

        this.isProcessing = true;
        this.helper.loading('로그인중입니다');
        this.authService.login(this.loginForm.value,(data)=>{
            this.authService.accessUserInfo((data)=>{
                this.isProcessing = false;
                this.helper.loaded();

                this.onLoginSuccess(data);
            },(error)=>{
                this.helper.toast('토큰이 만료 되었습니다.',3000,'top');
                this.isProcessing = false;
                this.helper.loaded();
            });

        },(error)=>{
            this.helper.toast('아이디 혹은 비밀번호를 확인해주세요.',3000,'top');
            this.isProcessing = false;
            this.helper.loaded();
      });

    }

    public moveRegisterPage():void{
        this.navCtrl.push(this.$PAGE.REGISTER)
    }

    public onRoot():void{
        this.navCtrl.setRoot(this.$PAGE.HOME);
    }

    public onBack():void{
        this.navCtrl.pop();
    }

    //로그인 성공
    public onLoginSuccess(user:User):void{
        let callback = this.navParams.get('callback');
        callback(user).then(()=>{
            this.place.set('cid',user.Email);
            this.navCtrl.pop();
        });
    }
}
