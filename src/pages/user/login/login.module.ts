import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from 'ionic-angular';
import {LoginPage} from "./login";
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
    imports: [
        CommonModule,
        ComponentsModule,
        IonicPageModule.forChild(LoginPage),
    ],
    declarations: [
        LoginPage,
    ],
    providers: [
    ]
})
export class LoginModule {}
