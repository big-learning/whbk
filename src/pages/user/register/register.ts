import {Component} from "@angular/core";
import {IonicPage, NavController} from "ionic-angular";
import {AuthPage} from "../../page";
import {RegisterServiceProvider} from "../../../providers/service/register.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HelperProvider} from "../../../providers/helper/helper";
import {AuthServiceProvider} from "../../../providers/service/auth.service";

@IonicPage({name: 'register', defaultHistory:['home']})
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})

export class RegisterPage extends AuthPage{

  private joinForm:FormGroup = null;

  constructor(
      private registerService:RegisterServiceProvider,
      protected authService:AuthServiceProvider,
      private formBuilder: FormBuilder,
      private navCtrl:NavController,
      private helper:HelperProvider
  ){
    super(authService);

      this.joinForm = this.formBuilder.group({
          Email:['', Validators.required],
          Name:['',Validators.required],
          Password:['',Validators.required],
          Password_confirmation:['',Validators.required]
      });
  }

    public onRegister(){

        const obj = this;
        this.isProcessing = true;


        this.helper.loading('잠시만 기다려주세요');

        this.registerService.register(this.joinForm.value,(data)=>{
            this.authService.accessUserInfo((data)=>{
                this.isProcessing = false;
                this.helper.loaded();


                this.helper.alert('',data['Name'] + '님 가입 완료되었습니다.',function(){
                    obj.navCtrl.parent.select(2);
                    obj.navCtrl.push(obj.$PAGE.USER_SEL_CATEGORY);
                });


            },(error)=>{
                this.isProcessing = false;
                this.helper.loaded();
            });
        },(error)=>{
            this.helper.toast(error.massage,3000,'top');
            this.isProcessing = false;
            this.helper.loaded();
        });
        //this.navCtrl.pop();
    }

    public onBack(){
        this.navCtrl.pop();
    }

}
