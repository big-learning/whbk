import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {RegisterPage} from "./register";
import {IonicPageModule} from "ionic-angular";
import {RegisterServiceProvider} from "../../../providers/service/register.service";
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
  imports: [
      CommonModule,
      ComponentsModule,
      IonicPageModule.forChild(RegisterPage),
  ],
  declarations: [
      RegisterPage,
  ],
  entryComponents: [
      RegisterPage,
  ],
  providers: [
      RegisterServiceProvider,
  ]
})
export class RegisterModule {}
