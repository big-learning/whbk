import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from 'ionic-angular';
import {Mypage} from "./mypage";
import {ComponentsModule} from "../../../components/components.module";
import {UserSelCategory} from "./selcategory/user-sel-category";

@NgModule({
    imports: [
        CommonModule,
        ComponentsModule,
        IonicPageModule.forChild(Mypage),
    ],
    declarations: [
        Mypage,
    ],
    providers: [
    ],
})
export class LoginModule {}
