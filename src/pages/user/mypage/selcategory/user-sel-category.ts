import {Component, OnInit} from "@angular/core";
import {BasePage} from "../../../page";
import {IonicPage, NavController, ViewController} from "ionic-angular";
import {HelperProvider} from "../../../../providers/helper/helper";
import {GroupCategoryServiceProvider} from "../../../../providers/service/group/group-category.service";
import {GroupCategory} from "../../../../models/group/group";
import {AuthServiceProvider} from "../../../../providers/service/auth.service";

@IonicPage({name: 'user-sel-category'})
@Component({
  selector: 'user-sel-category',
  templateUrl: 'user-sel-category.html'
})

export class UserSelCategory extends BasePage implements OnInit{

    private categoryList:Array<GroupCategory> = null;
    private temp:Array<GroupCategory> = null;

    private selectedCategory:Object = {};

    constructor(
        private helper:HelperProvider,
        protected authService:AuthServiceProvider,
        private userCateService:GroupCategoryServiceProvider,
        private viewCrtl:ViewController
    ){
        super(authService);

        //카테고리 리스트
        this.userCateService.categoryList((result)=>{
            this.categoryList = result.category !== null ? result.category : [];
            this.temp = [...this.categoryList];
        });

        //유저 선택된 카테고리 리스트
        this.userCateService.selectedCategoryList((result)=>{
            if(result.category !== null){
                for(let i in result.category){
                    this.selectedCategory[result.category[i]['CateId02']] = result;
                }
            }
        });
    }

    public ngOnInit():void{
        //this.user = this.helper.place('user');
    }

    public dismiss(datas:Object = {}):void{
        //this.helper.modalClose();
        this.viewCrtl.dismiss(datas);
    }

    //카테고리 검색
    public searchCategory($event):void{
        let val = $event.target.value;

        this.categoryList = this.temp.filter((item) => {
            const _item = item.CateName01+' / '+item.CateName02;
            return _item.indexOf(val) !== -1;
        });
    }

    //선택
    public onSelectToggle(category:GroupCategory){
        if(this.selectedCategory.hasOwnProperty(category.CateId02)){
            delete this.selectedCategory[category.CateId02];
        }else{
            this.selectedCategory[category.CateId02] =  category;
        }
    }


    public saveCategory(){

        this.helper.loading('저장중 입니다.');
        this.userCateService.saveSelectedCategory(Object.keys(this.selectedCategory),(result)=>{
            this.helper.loaded();
            if(result.status){
                this.viewCrtl.dismiss(result);
                //this.helper.modalClose();
            }
        });
    }

    //선택된 아이템 개수
    private seletedCount():number{
        return Object.keys(this.selectedCategory).length;
    }

}
