import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from "../../../../components/components.module";
import {GroupCategoryServiceProvider} from "../../../../providers/service/group/group-category.service";
import {UserCategory} from "./user-category";
import {AuthServiceProvider} from "../../../../providers/service/auth.service";

@NgModule({
    imports: [
        CommonModule,
        ComponentsModule,
        IonicPageModule.forChild(UserCategory),
    ],
    declarations: [
        UserCategory
    ],
    providers: [
        GroupCategoryServiceProvider,
        AuthServiceProvider
    ]
})
export class UserSelCategoryModule {}
