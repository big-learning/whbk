import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from "../../../../components/components.module";
import {UserSelCategory} from "./user-sel-category";
import {GroupCategoryServiceProvider} from "../../../../providers/service/group/group-category.service";
import {AuthServiceProvider} from "../../../../providers/service/auth.service";

@NgModule({
    imports: [
        CommonModule,
        ComponentsModule,
        IonicPageModule.forChild(UserSelCategory),
    ],
    declarations: [
        UserSelCategory
    ],
    providers: [
        GroupCategoryServiceProvider,
        AuthServiceProvider
    ]
})
export class UserSelCategoryModule {}
