import {Component, OnInit} from "@angular/core";
import {BasePage} from "../../../page";
import {IonicPage, NavController, ViewController} from "ionic-angular";
import {HelperProvider} from "../../../../providers/helper/helper";
import {GroupCategoryServiceProvider} from "../../../../providers/service/group/group-category.service";
import {GroupCategory} from "../../../../models/group/group";
import {AuthServiceProvider} from "../../../../providers/service/auth.service";

@IonicPage({name: 'user-category'})
@Component({
  selector: 'user-category',
  templateUrl: 'user-category.html'
})

export class UserCategory extends BasePage implements OnInit{

    private categoryList:Array<GroupCategory> = null;
    private temp:Array<GroupCategory> = null;

    private selectedCategory:Object = {};

    constructor(
        private helper:HelperProvider,
        protected authService:AuthServiceProvider,
        private userCateService:GroupCategoryServiceProvider,
        private viewCrtl:ViewController
    ){
        super(authService);

        this.datalist();
    }

    public ngOnInit():void{
        //this.user = this.helper.place('user');
    }

    public datalist(){

        //카테고리 리스트
        this.userCateService.selectedCategoryList((result)=>{
            this.categoryList = result.category !== null ? result.category : [];
            this.temp = [...this.categoryList];
        });
    }



    //모달창 닫기
    public dismiss():void{
        this.viewCrtl.dismiss();
    }


    public move(page):void{
        this.helper.modalOpen(page,null, (data) => {
            if(data.hasOwnProperty('status') && data['status'] === 'success'){
                this.datalist();
            }
        });
    }

    //선택된 아이템 개수
    private seletedCount():number{
        return this.categoryList.length;
    }


}
