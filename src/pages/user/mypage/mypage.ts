import {Component, OnInit} from "@angular/core";
import {AlertController, IonicPage, Modal, NavController, ViewController} from "ionic-angular";
import {AuthPage, BasePage} from "../../page";
import {AuthServiceProvider} from "../../../providers/service/auth.service";
import {HelperProvider} from "../../../providers/helper/helper";
import {User} from "../../../models/user";

@IonicPage({name: 'mypage', defaultHistory:['home']})
@Component({
  selector: 'page-mypage',
  templateUrl: 'mypage.html'
})

export class Mypage extends BasePage implements OnInit{

    private user:User = null;

    constructor(
      private navCtrl:NavController,
      protected authService:AuthServiceProvider,
      private helper:HelperProvider
    ){
      super(authService);
    }

    public ngOnInit():void{
        this.user = this.helper.place('user');
        //this.helper.modalOpen(this.$PAGE.USER_SEL_CATEGORY);
    }

    //로그아웃
    public logout():void{
        this.authService.logout();
    }

    //페이지 이동
    public move(page):void{
        this.navCtrl.push(page);
    }

    //페이지 이동
    public pop(page):void{
        this.helper.modalOpen(page);
    }

    //페이지 이동 : 로그인페이지
    public moveLoginPage():void{
        this.navCtrl.push(this.$PAGE.LOGIN,{
            callback : (data)=>{
                return new Promise((res, rej) =>{
                    this.user = data;
                    res();
                });
            }
        });
    }

    //페이지 이동 : 회원가입페이지
    public moveJoinPage():void{
        this.navCtrl.push(this.$PAGE.REGISTER);
    }


}
