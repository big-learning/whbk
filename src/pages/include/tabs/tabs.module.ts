import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from "ionic-angular";
import {ComponentsModule} from "../../../components/components.module";
import {TabsPage} from "./tabs";

@NgModule({
  imports: [
      CommonModule,
      ComponentsModule,
      IonicPageModule.forChild(TabsPage)
  ],
  declarations: [
      TabsPage,
  ],
  exports:[
      TabsPage,
  ],

   entryComponents:[TabsPage]


})
export class StudyModule {}
