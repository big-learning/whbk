import {Component, Input, OnInit} from '@angular/core';
import {IonicPage, NavParams} from "ionic-angular";
import {PAGE} from "../../pages.constants";


@IonicPage({'name':'tabs'})
@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html'
})
export class TabsPage {

    private params = {};

    private $PAGE = PAGE;

    constructor(params: NavParams) {
        this.params = params;


        console.log(this.params); // returns NavParams {data: Object}
    }

}