import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from "ionic-angular";
import {ComponentsModule} from "../../../components/components.module";
import {CreateStudyPage} from "./create-study";
import {AuthServiceProvider} from "../../../providers/service/auth.service";

@NgModule({
  imports: [
      CommonModule,
      ComponentsModule,
      IonicPageModule.forChild(CreateStudyPage)
  ],
  declarations: [
      CreateStudyPage,
  ],
  exports:[
      CreateStudyPage,
  ],
  providers:[
      AuthServiceProvider
  ]
})
export class CreateStudyModule {}
