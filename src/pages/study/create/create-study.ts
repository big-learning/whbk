import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {BasePage} from "../../page";
import {HelperProvider} from "../../../providers/helper/helper";
import {GroupCategory} from "../../../models/group/group";
import {locations} from "../../../config/config";
import {AuthServiceProvider} from "../../../providers/service/auth.service";

@IonicPage({name:'create-study'})
@Component({
  selector: 'page-create-study',
  templateUrl: 'create-study.html'
})
export class CreateStudyPage extends BasePage{


    private locations:Array<Object> = locations;

    //지역
    private location = null;

    //분류
    private category:GroupCategory = null;

    //달성목표
    private goal = '';

    //커리큘럼
    private description = '';


    constructor(
        protected authService:AuthServiceProvider,
        public navCtrl: NavController,
        private helper:HelperProvider,
    ) {
        super(authService);
        this.location = this.locations[0]['key'];
        console.log(this.location);
    }

    //Create Next Level
    public next():void{

        let params = {
            location :this.location,
            category :this.category,
            goal :this.goal,
            description :this.description
        };

        this.navCtrl.push(this.$PAGE.CREATE_NEXT_STUDY, params);
    }

    //Open Category Popup & Callback
    private openCategory():void{
        this.helper.modalOpen(this.$PAGE.STUDY_CATEGORY,null, (data)=>{
            this.saveCategory(data);
        });
    }

    private saveCategory(category):void{
        for(let i in category){
            this.category = category[i];
        }
    }

    private onSelectedLocation(location):void{
        console.log(location);
    }

    // Button enable Y/N
    private isNext(){
        return this.location !== null && this.category !== null && this.goal !== '' && this.description !== '';
    }

}
