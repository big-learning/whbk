import { Component } from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';
import {BasePage} from "../../page";
import {HelperProvider} from "../../../providers/helper/helper";
import {Group} from "../../../models/group/group";
import {DateClass} from "../../../library/basics/basics";
import {GroupServiceProvider} from "../../../providers/service/group/group.service";
import {RootNavProvider} from "../../../providers/helper/root-nav";
import {AuthServiceProvider} from "../../../providers/service/auth.service";

@IonicPage({name:'create-next-study'})
@Component({
  selector: 'page-create-next-study',
  templateUrl: 'create-next-study.html'
})
export class CreateNextStudyPage extends BasePage{


    //시작날짜
    private startDate;

    //종료 날짜
    private period = 30;

    //최대 회원 수
    private memberMaxCount = 3;

    //그룹 생성 데이터
    private groupConfirmData:Group = null;

    constructor(
        private helper:HelperProvider,
        private navParams:NavParams,
        protected authService:AuthServiceProvider,
        private groupServiceProvider:GroupServiceProvider,
        private rootNav:RootNavProvider
    ) {
        super(authService);
        this.startDate = new DateClass().getFormatDate('Y-M-D');

        this.groupConfirmData = {
            GroupName :navParams.data.goal,
            GroupDescription :navParams.data.description,
            GroupLocation: navParams.data.location,
            GroupCateIdx: navParams.data.category.CateId02
        };
    }


    private createGroup(){

        this.helper.confirm('스터디 생성','이대로 생성하시겠습니까?',()=>{
            this.helper.loading('스터디를 생성중입니다.');
            this.groupServiceProvider.create(this.confirmDatas(),(data)=>{
                this.helper.loaded();
                this.rootNav.rootRoot(this.$PAGE.TABS);
            },(fail)=>{
                this.helper.loaded();
            });
        },
        ()=>{

        });
    }

    private confirmDatas(){
        this.groupConfirmData.GroupLimitMembers = this.memberMaxCount;
        this.groupConfirmData.GroupPeriod = this.period;
        this.groupConfirmData.GroupStartDatetime = this.startDate;

        return this.groupConfirmData;
    }


}

