import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from "ionic-angular";
import {ComponentsModule} from "../../../components/components.module";
import {CreateNextStudyPage} from "./create-next-study";
import {GroupServiceProvider} from "../../../providers/service/group/group.service";
import {RootNavProvider} from "../../../providers/helper/root-nav";
import {AuthServiceProvider} from "../../../providers/service/auth.service";

@NgModule({
  imports: [
      CommonModule,
      ComponentsModule,
      IonicPageModule.forChild(CreateNextStudyPage)
  ],
  declarations: [
      CreateNextStudyPage,
  ],
  exports:[
      CreateNextStudyPage,
  ],
  providers:[
      GroupServiceProvider,
      RootNavProvider,
      AuthServiceProvider
  ]
})
export class CreateNextStudyModule {}
