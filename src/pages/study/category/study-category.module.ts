import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from "../../../components/components.module";
import {GroupCategoryServiceProvider} from "../../../providers/service/group/group-category.service";
import {StudyCategory} from "./study-category";
import {AuthServiceProvider} from "../../../providers/service/auth.service";

@NgModule({
    imports: [
        CommonModule,
        ComponentsModule,
        IonicPageModule.forChild(StudyCategory),
    ],
    declarations: [
        StudyCategory
    ],
    providers: [
        GroupCategoryServiceProvider,
        AuthServiceProvider
    ]
})
export class StudyCategoryModule {}
