import {Component, OnInit} from "@angular/core";
import {BasePage} from "../../page";
import {IonicPage, ViewController} from "ionic-angular";
import {GroupCategoryServiceProvider} from "../../../providers/service/group/group-category.service";
import {GroupCategory} from "../../../models/group/group";
import {AuthServiceProvider} from "../../../providers/service/auth.service";

@IonicPage({name: 'study-category'})
@Component({
  selector: 'study-category',
  templateUrl: 'study-category.html'
})

export class StudyCategory extends BasePage implements OnInit{

    private categoryList:Array<GroupCategory> = null;
    private temp:Array<GroupCategory> = null;

    private selectedCategory:Object = {};

    constructor(
        protected authService:AuthServiceProvider,
        private userCateService:GroupCategoryServiceProvider,
        private viewCrtl:ViewController
    ){
        super(authService);

        //카테고리 리스트
        this.userCateService.categoryList((result)=>{
            this.categoryList = result.category !== null ? result.category : [];

            let current = -1;
            this.categoryList = this.cateRowStart(this.categoryList);
            this.temp = [...this.categoryList];
        });

    }

    public ngOnInit():void{}

    //카테고리 검색
    public searchCategory($event):void{
        let val = $event.target.value;

        this.categoryList = this.temp.filter((item) => {
            const _item = item.CateName01+' / '+item.CateName02;
            return _item.toLowerCase().indexOf(val.toLowerCase()) !== -1;
        });

        this.categoryList = this.cateRowStart(this.categoryList);
    }

    //선택
    public onSelectToggle(category:GroupCategory){
        if(this.selectedCategory.hasOwnProperty(category.CateId02)){
            delete this.selectedCategory[category.CateId02];
        }else{
            this.selectedCategory[category.CateId02] =  category;
        }
    }

    //카테고리 저장
    public saveCategory(){
        this.viewCrtl.dismiss(this.selectedCategory);
    }

    //카테고리 닫기
    public dismiss(datas:Object = {}):void{
        this.viewCrtl.dismiss(datas);
    }

    //선택된 아이템 개수
    private seletedCount():number{
        return Object.keys(this.selectedCategory).length;
    }


    private cateRowStart(categoryList){
        let current = -1;
        for(let i in categoryList){
            categoryList[i]['start'] = false;
            if(current != categoryList[i].CateId01) {
                categoryList[i]['start'] = true;
                current = categoryList[i].CateId01;
            }
        }

        return categoryList;
    }
}
