import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Group} from "../../../models/group/group";

@IonicPage({name:'study-view', segment:'study-view/:groupId'})
@Component({
  selector: 'page-study-view',
  templateUrl: 'study-view.html',
})
export class StudyViewPage {

  private groupIdx:number;
  private group:Group;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams
  ) {

      this.groupIdx = navParams.get('groupId');
      this.group = navParams.get('group');

      console.log(this.groupIdx,this.group);
  }

  ionViewDidLoad() {

  }

}
