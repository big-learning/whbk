import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudyViewPage } from './study-view';
import {CommonModule} from "@angular/common";
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
  declarations: [
    StudyViewPage,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    IonicPageModule.forChild(StudyViewPage),
  ],
})
export class StudyViewPageModule {}
