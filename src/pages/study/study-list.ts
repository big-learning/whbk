import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController} from 'ionic-angular';
import {BasePage} from "../page";
import {RootNavProvider} from "../../providers/helper/root-nav";
import {GroupServiceProvider} from "../../providers/service/group/group.service";
import {Group, GroupSearch} from "../../models/group/group";
import {AuthServiceProvider} from "../../providers/service/auth.service";
import {FormControl} from "@angular/forms";
import 'rxjs/add/operator/debounceTime';

@IonicPage({name:'study-list'})
@Component({
  selector: 'page-study-list',
  templateUrl: 'study-list.html'
})
export class StudyListPage extends BasePage{

    private segmentList = [
        {value:'all',name:'전체'},
        {value:'target',name:'맞춤형 스터디'},
        /*{value:'manto',name:'멘토 스터디'}*/
    ];
    private selectedSegment = this.segmentList[0].value;

    private loading = true;

    private groupList:Array<Group> = [];
    private searchControl: FormControl;


    private search:GroupSearch = {
        text : '',
        offset : 1,
        options: {}
    };


    constructor(
        public navCtrl: NavController,
        private rootNav:RootNavProvider,
        protected authService:AuthServiceProvider,
        private groupServiceProvider:GroupServiceProvider
    ) {
        super(authService);

        this.searchControl = new FormControl();
    }

    //ionic 뷰 로드
    public ionViewDidLoad() {

        this.updateGroup();
        this.searchControl.valueChanges.debounceTime(500).subscribe(search => {
            this.updateGroup();
        });
    }

    //스터디 생성 페이지
    public createGroup($event){
        this.rootNav.rootPush(this.$PAGE.CREATE_STUDY);
    }

    //스터디 디테일 페이지
    public studyLink(group){
        this.rootNav.rootPush(this.$PAGE.STUDY_VIEW,{group});
    }


    //스터디 리스트
    public list(callback = null){
        this.groupServiceProvider.list(this.search,(datas)=>{

            for(let i in datas['list']){
                let tempDateTime = datas['list'][i]['GroupStartDatetime'];
                datas['list'][i]['GroupStartDatetime'] = this.datetimeConvert(tempDateTime);
            }
            this.groupList = this.groupList.concat(datas['list']);
            this.loading = false;

            if(callback !== null){
                callback(datas['list']);
            }
        })
    }


    //추가 로딩
    doInfinite(infiniteScroll): Promise<any> {

        return new Promise((resolve) => {
            this.search.offset++;
            this.list((datas)=>{
                if (datas.length === 0) {
                    infiniteScroll.enable(false);
                } else {
                    infiniteScroll.enable(true);
                }
            });
        });
    }

    //상단 세그먼트 변경
    segmentUpdate($event) {
        console.log($event);
    }


    //검색
    private onSearchInput(){
        this.loading = true;
        this.search.offset = 1;
        this.groupList = [];
    }

    //리스트 초기화
    private updateGroup(){
        this.list();
    }

    //날짜 변경
    private datetimeConvert(tempDateTime){
        return tempDateTime.substring(0,4)+"년 "+tempDateTime.substring(5,7)+"월 "+tempDateTime.substring(6,8)+"일 ";
    }


}
