import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {IonicPageModule} from "ionic-angular";
import {ComponentsModule} from "../../components/components.module";
import {StudyListPage} from "./study-list";
import {RootNavProvider} from "../../providers/helper/root-nav";
import {GroupServiceProvider} from "../../providers/service/group/group.service";
import {AuthServiceProvider} from "../../providers/service/auth.service";

@NgModule({
  imports: [
      CommonModule,
      ComponentsModule,
      IonicPageModule.forChild(StudyListPage)
  ],
  declarations: [
      StudyListPage,
  ],
  exports:[
      StudyListPage,
  ],
  providers:[
      RootNavProvider,
      GroupServiceProvider,
      AuthServiceProvider
  ]
})
export class StudyListModule {}
