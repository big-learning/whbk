import {PAGE} from "./pages.constants";
import {AuthServiceProvider} from "../providers/service/auth.service";

export interface Page{}

export class BasePage{

    protected isProcessing = false;

    protected $PAGE = PAGE;

    public constructor(
        protected authService:AuthServiceProvider
    ){

    }

    //로그인 여부
    public isAuthenticated():boolean {
        return this.authService.authenticated();
    }

    public onBack(){

    }

    public onRoot(){

    }


}

export class AuthPage extends BasePage{

    protected isProcessing = false;
}