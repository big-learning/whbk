import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {AlertController, Events, LoadingController, ModalController, ToastController} from "ionic-angular";
import {PlaceFactory} from "../../library/storage/place-factory";

@Injectable()
export class HelperProvider {

  private loader = null;
  private modal = null;

  constructor(
      private alertCtrl  :AlertController,
      private toastCtrl  :ToastController,
      private loadingCtrl:LoadingController,
      private modalCtrl: ModalController,
      private events:Events
  ) {

  }

    public alert(title:string, content:string , callback = null) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: content,
            buttons: [{
                text: '확인',
                handler : () => {
                    if(callback !== null){
                        callback();
                    }
                }
            }]
        });
        alert.present();
    }

    public confirm(title:string, content:string , ok = null, cancal = null) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: content,
            buttons: [
                {
                    text: '취소',
                    handler : () => {
                        if(cancal !== null){
                            cancal();
                        }
                    }
                },
                {
                    text: '확인',
                    handler : () => {

                        if (ok !== null) {
                            ok();
                        }
                    }
                }
            ]
        });
        alert.present();
    }

    public toast(massage:string, time:number=3000, position:string='bottom'){
        let toast = this.toastCtrl.create({
            message: massage,
            duration: time,
            position: position,
            closeButtonText: '확인'
        });
        toast.present();
    }

    public loading(massage:string="로딩중..",time:number = 0):void {

        let options = {
            content: massage,
        };

        if(time > 0){
            options['duration'] = time
        }

        this.loader = this.loadingCtrl.create(options);
        this.loader.present();
    }

    public loaded():void {
        this.loader.dismiss();
    }


    public modalOpen(page:string, param:any = null, callback = null):void {
        this.modal = this.modalCtrl.create(page);
        this.modal.present();

        this.modal.onDidDismiss(data => {
            if(callback !== null){
                callback(data,'callback');
            }
        });
    }

    public modalClose(){
        this.modal.dismiss();
    }


    public place(key:string, type = 'local'){
        return PlaceFactory.instance().factory(type).get(key);
    }

    public eventPush(key:string, value:any){
        this.events.publish(key,value);
    }


}
