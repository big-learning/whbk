
import {Injectable} from "@angular/core";
import {Events} from "ionic-angular";

@Injectable()
export class RootNavProvider {

    constructor(
        private events:Events
    ){
    }

    public rootRoot(page, params = {}){
        this.events.publish('menu:root',{page:page, params:params});
    }

    public rootPush(page, params = {}){
        this.events.publish('menu:push',{page:page, params:params});
    }

    public rootPop(){
        this.events.publish('menu:pop');
    }

}
