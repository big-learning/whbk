import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {User} from "../../models/user";
import {HttpServiceFactory} from "../http/auth/auth-authentication-factory";
import {Authenticatable} from "../http/auth/authenticatable.interface";
import {PlaceFactory} from "../../library/storage/place-factory";
import {PlaceInterface} from "../../library/storage/place.interface";

@Injectable()
export class AuthServiceProvider {

  private auth:Authenticatable<Object> = null;
  private place:PlaceInterface = null;

  constructor(
      public http: Http,
      private httpServiceFactory:HttpServiceFactory
  ) {
      this.auth = this.httpServiceFactory.factory('oauth');
      this.place = PlaceFactory.instance().factory('local');
  }

    //로그인
    public login(user:User, success, fail  = null) : void {
        this.auth.authenitication(user.Email,user.Password).subscribe((data)=>{

            this.place.set('token',data);

            success(data);

        },(error) => {
            const result = JSON.parse(error._body);
            if(fail !== null ) fail(result);
        });
    }

    //로그인한 유저 정보 가져오기
    public accessUserInfo(success, fail  = null){

        this.auth.user().subscribe((data)=>{

            this.place.set('user',data);

            success(data);

        },(error) => {
            if(fail !== null ) fail(error);
        });
    }

    //로그아웃
    public logout() : void {
        this.place.destory();
    }

    //로그인 여부 확인
    public authenticated() : boolean {
        return this.place.isValue('user');
    }
}
