import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {PlaceInterface} from "../../library/storage/place.interface";
import {HttpServiceFactory} from "../http/auth/auth-authentication-factory";
import {PlaceFactory} from "../../library/storage/place-factory";
import {ApiHttp} from "../http/api-http";

@Injectable()
export class RegisterServiceProvider {

    private place:PlaceInterface = null;
    private httpService:ApiHttp = null;

    constructor(
        private httpServiceFactory:HttpServiceFactory,
    ) {
        this.httpService = this.httpServiceFactory.factory('oauth');
        this.place = PlaceFactory.instance().factory('local');
    }

    public register(datas:Object, success, fail = null){

       this.httpService.post('/api/register',datas).subscribe((result)=>{

           this.place.set('token',result['token']);

           success(result);

       },(error)=>{
           const result = JSON.parse(error._body);

           if(fail !== null ) fail(result);
       });
    }

}
