import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {PlaceInterface} from "../../library/storage/place.interface";
import {PlaceFactory} from "../../library/storage/place-factory";

@Injectable()
export class AppServiceProvider {

    private place:PlaceInterface = null;

    constructor(
      public http: Http
    ){
      this.place = PlaceFactory.instance().factory('local');
    }

    public isExplain():boolean{
        return this.place.get('explain');
    }

    public appInit():void{
        console.log('앱 최초 실행');
    }

    public appStart():void{
        console.log('앱 시작');
    }

    public appEnd():void{
        console.log('앱 종료');
    }

}
