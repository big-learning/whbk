import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpServiceFactory} from "../http/auth/auth-authentication-factory";
import {PlaceFactory} from "../../library/storage/place-factory";
import {PlaceInterface} from "../../library/storage/place.interface";
import {ApiHttp} from "../http/api-http";
import {paramterConvertable} from "../http/paramter-convertable.interface";

@Injectable()
export class ServiceContent implements paramterConvertable{

  protected http:ApiHttp = null;
  protected place:PlaceInterface = null;

  constructor(
      protected httpServiceFactory:HttpServiceFactory
  ) {
      this.http = this.httpServiceFactory.factory('oauth');
      this.place = PlaceFactory.instance().factory('local');
  }

    public paramterConversion(paramter){
        return Object.keys(paramter).map( (key)=> key+'='+encodeURIComponent(paramter[key]) ).join('&');
    }

    protected errorHandler(error, callback = null):void{

      const result = JSON.parse(error._body);
      if(callback !== null ) callback(result);
    }

}
