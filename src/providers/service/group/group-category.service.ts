import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpServiceFactory} from "../../http/auth/auth-authentication-factory";
import {ServiceContent} from "../service-content";

@Injectable()
export class GroupCategoryServiceProvider  extends ServiceContent {

    constructor(
        protected factory:HttpServiceFactory
    ) {
        super(factory);
    }


  //카테고리 리스트
  public categoryList(success, fail = null){
      this.http.get('/api/category').subscribe((result) =>{
          success(result);
      },(error)=>{
          const result = JSON.parse(error._body);
          if(fail !== null ) fail(result);
      });
  }

  //유저가 선택한 카테고리 리스트
  public selectedCategoryList( success, fail = null){
      const user = this.place.get('user');
      this.http.get('/api/user/category/'+user.UserIdx).subscribe((result) =>{
          success(result);
      },(error)=>{
          const result = JSON.parse(error._body);
          if(fail !== null ) fail(result);
      });
  }

  //유저의 카테고리 저장하기
  public saveSelectedCategory(cateList:Array<string|number>, success, fail = null){
      const user = this.place.get('user');
      this.http.post('/api/user/category',{'userIdx':user.UserIdx,'groupCateIdx':cateList}).subscribe((result) =>{
          success(result);
      },(error)=>{
          const result = JSON.parse(error._body);
          if(fail !== null ) fail(result);
      });
  }


}
