import { Injectable } from '@angular/core';
import {HttpServiceFactory} from "../../http/auth/auth-authentication-factory";
import {ServiceContent} from "../service-content";
import {Group, GroupSearch} from "../../../models/group/group";
import {Base64} from "../../../library/basics/basics";

@Injectable()
export class GroupServiceProvider extends ServiceContent {

  constructor(
      protected factory:HttpServiceFactory
  ) {
      super(factory);
  }


    //그룹 리스트
    public list(options:GroupSearch, success, fail = null){
        let search =this.paramterConversion(options);
        this.http.get('/api/group?' + search).subscribe((result) =>{
            success(result);
        },(error)=>{
            this.errorHandler(error,fail);
        });
    }

  //그룹 가져오기
  public view(groupId, success, fail = null){
      this.http.get('/api/group/'+groupId).subscribe((result) =>{
          success(result);
      },(error)=>{
          this.errorHandler(error,fail);
      });
  }

  //유저가 선택한 카테고리 리스트
  public selectedCategoryList( success, fail = null){
      const user = this.place.get('user');
      this.http.get('/api/user/category/'+user.UserIdx).subscribe((result) =>{
          success(result);
      },(error)=>{
          this.errorHandler(error,fail);
      });
  }

  //그룹 생성
  public create(params:Group, success, fail = null){
      params = Object.assign({},params, this.place.get('user'));
      this.http.post('/api/group',params).subscribe((result) =>{
          success(result);
      },(error)=>{
          this.errorHandler(error,fail);
      });
  }


}
