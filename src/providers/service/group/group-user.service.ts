import { Injectable } from '@angular/core';
import {HttpServiceFactory} from "../../http/auth/auth-authentication-factory";
import {ServiceContent} from "../service-content";
import {Group} from "../../../models/group/group";

@Injectable()
export class GroupUserServiceProvider extends ServiceContent {

  constructor(
      protected factory:HttpServiceFactory
  ) {
      super(factory);
  }


    //유저의 그룹 리스트
    public list(success, fail = null){
        const user = this.place.get('user');
        this.http.get('/api/group/user/'+user.UserIdx).subscribe((result) =>{
            success(result);
        },(error)=>{
            this.errorHandler(error,fail);
        });
    }

}
