import {Headers, Http, RequestOptions} from "@angular/http";
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ApiHttpInterface} from "./api-http.interface";
import {paramterConvertable} from "./paramter-convertable.interface";


export  class ApiHttp implements ApiHttpInterface, paramterConvertable{

    protected API_URL;

    constructor(
        private http: Http
    ){
    }

    public get(rest_url:string, header?:string ){
        return this.http.get(this.API_URL+rest_url,this.defaultHeader()).map( res => res.json() );
    }

    public post(rest_url:string, parameter:Object, header?:string ) {
        return this.http.post(this.API_URL + rest_url, parameter, this.defaultHeader()).map( res => res.json() );
    }

    public put(rest_url:string, parameter:Object, header?:string ) {

        return this.http.put(this.API_URL+rest_url, this.paramterConversion(parameter), this.defaultHeader()).map( res => res.json() );
    }

    public delete(rest_url:string,  header?:string ){
        return this.http.delete(this.API_URL+rest_url, this.defaultHeader()).map( res => res.json() );
    }

    protected addheader(headers:Headers):Headers|null{return null};


    //API Header
    public defaultHeader(options:Object = {}):RequestOptions{

        let headerGroup = {};

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'x-requested-with Content-Type');
        headers.append('Accept', 'application/json');
        const addheaders = this.addheader(headers);
        if(addheaders !== null) headers = addheaders;
        headerGroup['headers'] = headers;

        return new RequestOptions(headerGroup);
    }

    //POST GET
    public paramterConversion(paramter){
        return Object.keys(paramter).map( (key)=> key+'='+encodeURIComponent(paramter[key]) ).join('&');
    }
}