import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Authenticatable} from "./authenticatable.interface";
import {ApiHttp} from "../api-http";
import {auth} from "../../../config/api";
import {PlaceFactory} from "../../../library/storage/place-factory";
import {Headers, Http, RequestOptions} from "@angular/http";
import {ENV} from "../../../config/environment.dev";


export class OAuthApiHttp extends ApiHttp implements Authenticatable<Object>{

    protected API_ID_TOKEN_KEY = 'token';
    protected API_URL = ENV.API_URL;

    public constructor(http:Http){
        super(http);
    }


    public getToken():string{

        return PlaceFactory.instance().factory('local').get(this.API_ID_TOKEN_KEY) || '';
    }

    // 유저의 고유 API 토큰 저장
    public setUserToken(token:string):void{
        localStorage.setItem(this.API_ID_TOKEN_KEY,token);
    }

    //인증
    public authenitication(id:string, password:string){

        return this.post(auth.OAUTH.AUTHENITICATION, this.oauthFormDatas(id,password));
    }

    public user(){
        return this.get('/api/user');
    }

    private oauthFormDatas(id:string, password:string):Object{
        return {
            'grant_type':auth.OAUTH.GRANT_TYPE,
            'client_id':auth.OAUTH.CLIENT_ID,
            'client_secret':auth.OAUTH.CLIENT_SECRET,
            'username':id,
            'password':password,
        };
    }


    //API 통신시 기본 Header 세팅
    protected addheader(headers:Headers):Headers{
        if(this.getToken()){
            headers.append('Authorization', 'Bearer '+this.getToken()['access_token']);
        }
        return headers;
    }


}