
import {Observable} from "rxjs/Observable";

export interface Authenticatable<T>{

    getToken():string
    authenitication(email:string,password:string):Observable<T>
    user():Observable<T>

}