import {Headers} from "@angular/http";
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Authenticatable} from "./authenticatable.interface";
import {auth} from "../../../config/api";
import {ApiHttp} from "../api-http";
import {ENV} from "../../../config/environment.dev";


export class JwtApiHttp extends ApiHttp implements Authenticatable<Object>{

    protected API_ID_TOKEN_KEY = 'JWT';
    protected API_URL = ENV.API_URL;

    public getToken():string{
        return localStorage.getItem(this.API_ID_TOKEN_KEY) || '';
    }

    // 유저의 고유 API 토큰 저장
    public setUserToken(token:string):void{
        localStorage.setItem(this.API_ID_TOKEN_KEY,token);
    }

    //인증
    public authenitication(id:string, password:string){
        return this.post(auth.JWT.AUTHENITICATION, {id:id, password:password});
    }

    public user(){
        return this.get('/api/user');
    }

    //API 통신시 기본 Header 세팅
    protected addheader(headers:Headers):Headers{

        if(this.getToken()){
            headers.append('Authorization', this.API_ID_TOKEN_KEY+' '+this.getToken());
        }
        return headers;
    }
}