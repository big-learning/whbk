import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {JwtApiHttp} from "./jwt-api-http";
import {OAuthApiHttp} from "./oauth-api-http";
import {Factory} from "../../../library/basics/basics";
import {ApiHttp} from "../api-http";
import {Authenticatable} from "./authenticatable.interface";


@Injectable()
export class HttpServiceFactory implements Factory<Authenticatable<Object>>{

    constructor(private http:Http){}

    private object = {};

    public factory(auth:string):any{

        if(this.object.hasOwnProperty(auth)) return this.object[auth];

        let object = null;

        switch (auth){

            case 'jwt':
                object = new JwtApiHttp(this.http);
                break;
            case 'oauth':
                object = new OAuthApiHttp(this.http);
                break;
            default:
                break;
        }

        if(object === null ) return false;

        this.object[auth] = object;

        return object;
    }

}