import {Component, OnDestroy, ViewChild} from '@angular/core';
import {Nav, Platform, Events} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AppServiceProvider} from "../providers/service/app.service";
import {BasePage} from "../pages/page";
import {AuthServiceProvider} from "../providers/service/auth.service";

@Component({
  templateUrl: 'app.html'
})
export class MyApp extends BasePage implements OnDestroy {

    public rootPage:any = this.$PAGE.TABS;
    @ViewChild(Nav) navCtrl: Nav;

    constructor(
      private platform: Platform,
      private statusBar: StatusBar,
      private splashScreen: SplashScreen,
      private events:Events,
      protected authServiceProvider:AuthServiceProvider,
      private AppServiceProvider:AppServiceProvider
    ) {
        super(authServiceProvider);
        this.initializeApp();
    }

    public initializeApp() {
        this.AppServiceProvider.appInit();
        this.platform.ready().then(() => {

            this.statusBar.styleDefault();
            this.splashScreen.hide();

            this.AppServiceProvider.appStart();

            if(this.AppServiceProvider.isExplain() === null){
                this.navCtrl.setRoot(this.$PAGE.TUTORIAL)

            }
        });

        this.listenToMenuActive();
    }

    //루트 메뉴 클릭 이벤트
    private listenToMenuActive() {
        this.events.subscribe('menu:push', (data) => {
            this.navCtrl.push(data.page,data.params || {});
        });

        this.events.subscribe('menu:pop', (data) => {
            this.navCtrl.pop();
        });

        this.events.subscribe('menu:root', (data) => {
            this.navCtrl.setRoot(data.page,data.params || {});
        });
    }



    public ngOnDestroy(){
        this.AppServiceProvider.appEnd();
    }
}

