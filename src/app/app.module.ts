import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import {HttpModule} from "@angular/http";
import {HttpServiceFactory} from "../providers/http/auth/auth-authentication-factory";
import {AuthServiceProvider} from "../providers/service/auth.service";
import {ComponentsModule} from "../components/components.module";
import { HelperProvider } from '../providers/helper/helper';
import {CommonModule} from "@angular/common";
import {AppServiceProvider} from "../providers/service/app.service";


@NgModule({
  imports: [
    HttpModule,
    BrowserModule,
    ComponentsModule,
    CommonModule,
    IonicModule.forRoot(MyApp,{tabsPlacement: 'bottom'}),
  ],
  declarations: [
      MyApp,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthServiceProvider,
    AppServiceProvider,
    HttpServiceFactory,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HelperProvider,
  ]
})
export class AppModule {}
