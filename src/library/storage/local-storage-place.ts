import {PlaceInterface} from "./place.interface";

export class LocalStoragePlace implements PlaceInterface {

    public get($key:string){
        return JSON.parse(localStorage.getItem($key));
    }

    public set($key,value){
        localStorage.setItem($key, JSON.stringify(value));
    }

    public all(){
        return localStorage;
    }

    public destory($key = null) {
        if($key === null){
            localStorage.clear();
        }else{
            localStorage.removeItem($key);
        }
    }

    public isValue($key){
        if(localStorage.getItem($key) && typeof localStorage.getItem($key) !== "undefined"){
            return true;
        }else{
            return false;
        }
    }


}