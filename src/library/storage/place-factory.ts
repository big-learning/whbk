import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Factory, Singleton} from "../basics/basics";
import {LocalStoragePlace} from "./local-storage-place";
import {PlaceInterface} from "./place.interface";
import {CookiePlace} from "./cookie-place";


export class PlaceFactory implements Factory<PlaceInterface>{

    private static _instance:PlaceFactory;

    private object = {};

    private constructor(){}

    public static instance(){
        PlaceFactory._instance = PlaceFactory._instance || new PlaceFactory();

        return PlaceFactory._instance;
    }

    public factory(key:string):PlaceInterface{

        if(this.object.hasOwnProperty(key)) return this.object[key];

        let object = null;

        switch (key){

            case 'local':
                object = new LocalStoragePlace();
                break;
            case 'cookie':
                object = new CookiePlace();
                break;
            case 'session':
                break;
            default:
                break;
        }

        if(object === null ) return null;

        this.object[key] = object;

        return object;
    }

}