
export interface PlaceInterface {
    get(key:string):any;

    set(key:string,value):void;

    all():Object;

    destory():void;

    isValue(key:string):boolean

}