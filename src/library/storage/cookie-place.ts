import {PlaceInterface} from "./place.interface";

export class CookiePlace implements PlaceInterface {

    public get($key:string){
        return this.getCookie($key);
    }

    public set($key,value):void{
        this.setCookie($key, value, 365);
    }

    public all(){
        return localStorage;
    }

    public destory($key = null) {
        if($key === null){
            //localStorage.clear();
        }else{
            this.setCookie($key,'',0);
        }
    }

    public isValue($key){
        if(localStorage.getItem($key) && typeof localStorage.getItem($key) !== "undefined"){
            return true;
        }else{
            return false;
        }
    }

    private setCookie(cName, cValue, cDay){
        let expire = new Date();
        let cookies = '';

        expire.setDate(expire.getDate() + cDay);
        cookies = cName + '=' + JSON.stringify(cValue) + '; path=/ '; // 한글 깨짐을 막기위해 escape(cValue)를 합니다.
        if(typeof cDay != 'undefined') cookies += ';expires=' + expire['toGMTString']() + ';';
        document.cookie = cookies;
    }

    // 쿠키 가져오기
    private getCookie(cName){
        cName = cName + '=';
        let cookieData = document.cookie;
        let start = cookieData.indexOf(cName);
        let cValue = '';
        if(start != -1){
            start += cName.length;
            let end = cookieData.indexOf(';', start);
            if(end == -1)end = cookieData.length;
            cValue = cookieData.substring(start, end);
        }

        return cValue ==='' ? '' : JSON.parse(cValue);
    }


}