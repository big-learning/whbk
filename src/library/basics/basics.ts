
export interface Factory<T>{
    factory($key:string):T;
}

export class Singleton{
    public static instance(){

    };
}

export class DateClass{

    private date;
    private dateFormat = 'Y-M-D';

    private year = 'Y';
    private month = 'M';
    private day = 'D';
    private hour = 'h';
    private min = 'i';
    private second = 's';

    public constructor(){
        this.date = new Date();
    }

    public format(format){
        this.dateFormat = format;
        return this;
    }

    public getFormatDate(format = null){
        let tempFormat = format === null ? this.dateFormat : format;

        while(true){
            if(tempFormat.indexOf(this.year) !== -1 ){
                tempFormat = tempFormat.replace(this.year,this.date.getFullYear());
                continue;
            }

            if(tempFormat.indexOf(this.month) !== -1 ){
                let month = this.date.getMonth()+1;
                month = month.toString().length == 1 ? '0'+month : month;
                tempFormat = tempFormat.replace(this.month,month);
                continue;
            }

            if(tempFormat.indexOf(this.day) !== -1 ){
                let date = this.date.getDate();
                date = date.toString().length == 1 ? '0'+date : date;
                tempFormat = tempFormat.replace(this.day,date);
                continue;
            }
            break;
        }

        return tempFormat;
    }
}


export const Base64 = {

    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
        let output = "";
        let chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        let i = 0;

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode : function (input)
    {
        let output = "";
        let chr1, chr2, chr3;
        let enc1, enc2, enc3, enc4;
        let i = 0;

        input = input.replace(/[^A-Za-z0-9+/=]/g, "");

        while (i < input.length)
        {
            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }

        return output;
    }
};