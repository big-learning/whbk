import {Component, Input} from '@angular/core';

@Component({
  selector: 'fab-btn',
  templateUrl: 'fab.component.html'
})
export class FabComponent {

  text: string;

  @Input('click') clickListener = null;

  @Input('bottom') bottom = false;
  @Input('top') top = false;
  @Input('right') right = false;
  @Input('left') left = false;

  constructor() {
  }



  //이벤트 리스너
  public onClickEventListener($event){
      if(this.clickListener !== null){
        this.clickListener($event);
      }
  }

}
