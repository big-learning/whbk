import {Component, Input, OnInit} from '@angular/core';
import {AlertController, NavController} from "ionic-angular";
import {AuthServiceProvider} from "../../../providers/service/auth.service";

@Component({
    selector: 'header-component',
    templateUrl: 'header.component.html'
})
export class HeaderComponent implements OnInit {

    @Input('title') title:string;
    @Input('search') search:boolean = false;
    @Input('color') color:string = 'default';
    @Input('searchEventListenr')  searchEventListenr = null;

    constructor(public navCtrl: NavController, public authService: AuthServiceProvider, public alertCtrl:AlertController) {
        this.title = '레알터디';
    }

    public ngOnInit(){

    }

}