import { NgModule } from '@angular/core';
import {IonicModule} from "ionic-angular";
import {HeaderComponent} from "./template/header/header.component";
import { FabComponent } from './fab/fab.component';

@NgModule({
	declarations: [
		HeaderComponent,
    	FabComponent
	],
	imports: [IonicModule],
	exports: [
		HeaderComponent,
    	FabComponent,
	]
})
export class ComponentsModule {}
