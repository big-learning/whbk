import {Component, Input, Output} from '@angular/core';

@Component({
  selector: 'study-item',
  templateUrl: 'study-item.component.html'
})
export class FabComponent {

  text: string;

  @Input('study') clickListener = null;

  //@Output('clickEvent') clickListener = new Em

  constructor() {
  }



  //이벤트 리스너
  public onClickEventListener($event){
      if(this.clickListener !== null){
        this.clickListener($event);
      }
  }

}
