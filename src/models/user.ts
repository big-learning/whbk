

export interface User{

    UserLevel:string;
    UserType:string;
    Email:string;
    Name:string;
    Password?:string;
}

