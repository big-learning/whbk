export interface PageInterface {
    name: string;
    shortName?: string;
    page: string;
    icon: string;
    index?: number;
    tab?: string;
}
