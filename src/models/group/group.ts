export interface GroupCategory{
    CateId01:number;
    CateId02:number;
    CateName01:string;
    CateName02:string;
    GroupCateDescription:string;
    GroupCateName:string;
}

export interface Group{
    GroupIdx?:number;
    CateId01?:number;
    CateId02?:number;
    CateName01?:string;
    CateName02?:string;
    GroupCateDescription?:string;
    GroupCateName?:string;
    GroupCateIdx?:number;
    GroupLocation?:number;
    GroupStatus?:string;
    GroupName:string;
    GroupDescription?:string;
    GroupLimitMembers?:number;
    GroupStartDatetime?:string;
    GroupPeriod?:number;
    CreateDatetime?:string;
}

export interface GroupSearch{
    text? : string;
    offset? : number;
    limit? : number;
    options?: Object;
}